//
//  WrapperViewModel.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

class WrapperViewModel: ObservableObject {
    
    @Published var navigationPath = NavigationPath()
    
    lazy var schoolListViewModel = {
        SchoolsViewModel(router: self)
    }()
}
