//
//  WrapperView.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

struct WrapperView: View {
    @StateObject var router = WrapperViewModel()
    var body: some View {
        NavigationStack(path: $router.navigationPath) {
            SchoolsView(viewModel: router.schoolListViewModel)
        }
    }
}
