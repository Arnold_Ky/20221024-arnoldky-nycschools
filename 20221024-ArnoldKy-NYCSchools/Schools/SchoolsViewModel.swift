//
//  SchoolsViewModel.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

class SchoolsViewModel: ObservableObject {
    
    @Published var schools = [HighSchool]()
    @Published var isLoading = false
    @Published var showingNoSATAlert = false
    @Published var showingErrorAlert = false
    
    let router: WrapperViewModel
    init(router: WrapperViewModel) {
        self.router = router
        Task {
            do {
                try await fetch()
            } catch {
                await MainActor.run {
                    self.isLoading = false
                }
            }
        }
    }
    
    func fetch() async throws {
        
        await MainActor.run {
            self.isLoading = true
        }
        
        
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        guard let url = URL(string: urlString) else {
            await MainActor.run {
                self.isLoading = false
            }
            return
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        
        let highSchoolResponse = try JSONDecoder().decode([HighSchool].self, from: data)
        
        await MainActor.run {
            self.schools = highSchoolResponse
            self.isLoading = false
        }
    }
    
    func selectSchoolIntent(model: HighSchool) {
        Task {
            do {
                try await selectSchoolIntentBackground(model: model)
            } catch {
                await MainActor.run {
                    self.isLoading = false
                }
            }
        }
    }
    

    //https://data.cityofnewyork.us/resource/f9bf-2cp4.json
    
    func testScoresForSchool(model: HighSchool) async throws -> TestScores? {
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(model.dbn)"
        
        guard let url = URL(string: urlString) else {
            await MainActor.run {
                self.isLoading = true
            }
            return nil
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        
        let responseSAT = try JSONDecoder().decode([TestScores].self, from: data)
        
        var testScores: TestScores?
        for checkScores in responseSAT {
            if checkScores.dbn == model.dbn {
                testScores = checkScores
            }
        }
        
        return testScores
    }
    
    private func selectSchoolIntentBackground(model: HighSchool) async throws {
        
        await MainActor.run {
            self.isLoading = true
        }
        
        guard let testScores = try await testScoresForSchool(model: model) else {
            await MainActor.run {
                self.showingNoSATAlert = true
                self.isLoading = false
            }
            return
        }
        
        await MainActor.run {
            self.isLoading = false
            self.navigateToDetailPage(model: model, testScores: testScores)
        }
    }
    
    func navigateToDetailPage(model: HighSchool, testScores: TestScores) {
        let detailViewModel = DetailViewModel(router: router,
                                              school: model,
                                              testScores: testScores)
        
        router.navigationPath.append(detailViewModel)
    }
    
    func name(for school: HighSchool) -> String {
        return school.school_name ?? ""
    }
    
    func dbn(for school: HighSchool) -> String {
        return school.dbn
    }
    
}
