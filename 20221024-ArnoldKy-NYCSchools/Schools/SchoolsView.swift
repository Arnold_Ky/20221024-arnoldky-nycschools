//
//  SchoolsView.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

struct SchoolsView: View {
    
    @ObservedObject var viewModel: SchoolsViewModel
    
    var body: some View {
        VStack(spacing: 0) {
            HStack {
                Spacer()
            }
            ScrollView {
                ForEach(viewModel.schools) { school in
                    Button {
                        viewModel.selectSchoolIntent(model: school)
                    } label: {
                        schoolCell(viewModel.name(for: school), viewModel.dbn(for: school))
                    }
                }
                .padding(.top, 24)
                .padding(.bottom, 24)
            }
            Spacer()
        }
        .overlay(loader(isLoading: viewModel.isLoading))
        .navigationDestination(for: DetailViewModel.self) { detailViewModel in
            DetailView(viewModel: detailViewModel)
        }
        .alert("Sorry, no SAT scores are available.", isPresented: $viewModel.showingNoSATAlert) {
            Button("Okay") { }
        }
        .alert("Unable to load, network connectivity.", isPresented: $viewModel.showingErrorAlert) {
            Button("Okay") { }
        }
        
        
    }
    
    func schoolCell(_ text: String, _ tidbit: String) -> some View {
        HStack {
            VStack {
                HStack {
                Text(text)
                    .foregroundColor(.black)
                    .font(.system(size: 18))
                    .padding(.all, 8)
                    .padding(.leading, 12)
                    .lineLimit(3)
                    .multilineTextAlignment(.leading)
                    Spacer()
                }
                HStack {
                    Text(tidbit)
                        .foregroundColor(.gray)
                        .font(.system(size: 18))
                        .padding(.all, 8)
                        .padding(.leading, 12)
                        .lineLimit(3)
                    Spacer()
                }
                
            }
            Spacer()
            Image(systemName: "chevron.right")
                .foregroundColor(.gray)
                .font(.system(size: 24))
                .padding(.all, 8)
                .padding(.trailing, 8)
        }
        .shadowBox()
        .padding(.horizontal, 24)
        .padding(.vertical, 6)
    }
    
    @ViewBuilder
    func loader(isLoading: Bool) -> some View {
        if isLoading {
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    ZStack {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: .red))
                            .scaleEffect(CGSize(width: 2.0, height: 2.0))
                            .padding(.all, 32)
                    }
                    .background(RoundedRectangle(cornerRadius: 12).fill().foregroundColor(.black.opacity(0.85)))
                    Spacer()
                }
                Spacer()
            }
            .background(Color.black.opacity(0.85))
        }
    }
}
