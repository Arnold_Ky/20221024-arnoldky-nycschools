//
//  HighSchoolResponse.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import Foundation

struct HighSchool: Decodable {
    let dbn: String
    let school_name: String?
    let building_code: String?
    
    let primary_address_line_1: String?
    let city: String?
    let zip: String?
    let state_code: String?
    
    let school_email: String?
    let website: String?
}

extension HighSchool: Identifiable, Hashable {
    var id: String { dbn }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(dbn)
    }
}
