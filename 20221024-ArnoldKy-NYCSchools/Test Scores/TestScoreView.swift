//
//  TestScoreView.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

struct DetailView: View {
    @ObservedObject var viewModel: DetailViewModel
    var body: some View {
        VStack {
            HStack(spacing: 0) {
                Button { [weak viewModel] in
                    viewModel?.dismiss()
                } label: {
                    HStack(spacing: -1) {
                        Image(systemName: "chevron.backward")
                        Text("Back")
                    }
                    .foregroundColor(.blue)
                }
                Spacer()
                
            }
            .padding(.leading, 24)
            ScrollView {
                
                VStack {
                    
                    Spacer()
                        .frame(height: 12)
                    
                    HStack {
                        
                        Text(viewModel.name(for: viewModel.school))
                            .foregroundColor(.black)
                            .font(.system(size: 34))
                            .fixedSize(horizontal: false, vertical: true)
                        
                        Spacer()
                    }
                    .padding(.horizontal, 24)
                    
                    VStack {
                        HStack {
                            Spacer()
                        }
                        VStack {
                            Text(viewModel.addressFirstLine(for: viewModel.school))
                                .foregroundColor(.black)
                                .font(.system(size: 18).bold())
                            Text(viewModel.addressSecondLine(for: viewModel.school))
                                .foregroundColor(.black)
                                .font(.system(size: 18).bold())
                        }
                        
                        .padding(.horizontal, 24)
                        .padding(.vertical, 6)
                        
                    }
                    .padding(.bottom, 12)
                    .padding(.horizontal, 24)
                    .shadowBoxDark()
                    .padding(.horizontal, 24)
                    
                    Spacer()
                        .frame(height: 16)
                    
                    VStack {
                        HStack {
                            Spacer()
                            Text("SAT Scores")
                                .foregroundColor(.black)
                                .font(.system(size: 32))
                            Spacer()
                        }
                        .padding(.top, 16)
                        VStack {
                            HStack {
                                Text("Students: ")
                                    .font(.system(size: 18))
                                    .padding(.leading, 8)
                                    .padding(.vertical, 4)
                                Text(viewModel.numberOfStudents(for: viewModel.testScores))
                                    .font(.system(size: 18))
                                Spacer()
                                
                            }
                            .padding(.all, 6)
                            .dataCell()
                            
                            HStack {
                                Text("Math: ")
                                    .font(.system(size: 18))
                                    .padding(.leading, 8)
                                    .padding(.vertical, 4)
                                Text(viewModel.math(for: viewModel.testScores))
                                    .font(.system(size: 18))
                                Spacer()
                                
                            }
                            .padding(.all, 6)
                            .dataCell()
                            
                            HStack {
                                Text("Reading: ")
                                    .font(.system(size: 18))
                                    .padding(.leading, 8)
                                    .padding(.vertical, 4)
                                Text(viewModel.criticalReading(for: viewModel.testScores))
                                    .font(.system(size: 18))
                                Spacer()
                                
                            }
                            .padding(.all, 6)
                            .dataCell()
                            
                            HStack {
                                Text("Writing: ")
                                    .font(.system(size: 18))
                                    .padding(.leading, 8)
                                    .padding(.vertical, 4)
                                
                                Text(viewModel.writing(for: viewModel.testScores))
                                    .font(.system(size: 18))
                                Spacer()
                            }
                            .padding(.all, 6)
                            .dataCell()
                        }
                        .foregroundColor(.gray)
                        .padding(.vertical, 6)
                    }
                    .padding(.bottom, 12)
                    .padding(.horizontal, 24)
                    .shadowBox()
                    .padding(.horizontal, 24)
                    
                    Spacer()
                }
                Spacer()
            }
        }
        .background(Color(red: 0.96, green: 0.96, blue: 0.96))
        .toolbar(.hidden, for: .navigationBar)
    }
}
