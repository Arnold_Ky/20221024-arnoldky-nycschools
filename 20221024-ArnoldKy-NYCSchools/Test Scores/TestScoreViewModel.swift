//
//  TestScoreViewModel.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

class DetailViewModel: ObservableObject {
    
    let router: WrapperViewModel
    let school: HighSchool
    let testScores: TestScores
    
    init(router: WrapperViewModel,
         school: HighSchool,
         testScores: TestScores) {
        self.router = router
        self.school = school
        self.testScores = testScores
    }
    
    func name(for school: HighSchool) -> String {
        return school.school_name ?? ""
    }
    
    func dbn(for school: HighSchool) -> String {
        return school.school_name ?? ""
    }
    
    func addressFirstLine(for school: HighSchool) -> String {
        return school.primary_address_line_1 ?? ""
    }
    
    func addressSecondLine(for school: HighSchool) -> String {
        let part1 = school.city ?? ""
        let part2 = school.state_code ?? ""
        let part3 = school.zip ?? ""
        return "\(part1), \(part2) \(part3)"
    }
    
    func numberOfStudents(for sat: TestScores) -> String {
        return sat.num_of_sat_test_takers ?? ""
    }
    
    func criticalReading(for sat: TestScores) -> String {
        return sat.sat_critical_reading_avg_score ?? ""
    }
    
    func math(for sat: TestScores) -> String {
        return sat.sat_math_avg_score ?? ""
    }
    
    func writing(for sat: TestScores) -> String {
        return sat.sat_writing_avg_score ?? ""
    }
    
    func dismiss() {
        router.navigationPath.removeLast()
    }
}

extension DetailViewModel: Hashable {
    static func == (lhs: DetailViewModel, rhs: DetailViewModel) -> Bool {
        lhs.school.id == rhs.school.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(school.id)
    }
}
