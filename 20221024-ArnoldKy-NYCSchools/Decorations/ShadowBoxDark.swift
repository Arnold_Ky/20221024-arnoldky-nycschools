//
//  ShadowBoxDark.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

struct ShadowBoxDark: ViewModifier {
    func body(content: Content) -> some View {
        content
            .background(RoundedRectangle(cornerRadius: 12).fill().foregroundColor(
                Color(red: 0.80, green: 0.80, blue: 0.80)
                )
                .shadow(color: .black.opacity(0.4), radius: 3, x: -1, y: 2)
            )
            .background(RoundedRectangle(cornerRadius: 12).stroke().foregroundColor(
                Color(red: 0.65, green: 0.65, blue: 0.65)
            ))
    }
}

extension View {
    func shadowBoxDark() -> some View {
        modifier(ShadowBoxDark())
    }
}
