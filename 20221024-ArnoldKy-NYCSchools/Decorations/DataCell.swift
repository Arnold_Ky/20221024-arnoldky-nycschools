//
//  ShadowBoxDark.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

struct DataCell: ViewModifier {
    func body(content: Content) -> some View {
        content
            .background(RoundedRectangle(cornerRadius: 4).fill().foregroundColor(Color(red: 0.93, green: 0.93, blue: 0.93)))
            .background(RoundedRectangle(cornerRadius: 4).stroke().foregroundColor(
            
                Color(red: 0.85, green: 0.85, blue: 0.85)
            ))
            .foregroundColor(Color(red: 0.125, green: 0.125, blue: 0.125))
    }
}

extension View {
    func dataCell() -> some View {
        modifier(DataCell())
    }
}
