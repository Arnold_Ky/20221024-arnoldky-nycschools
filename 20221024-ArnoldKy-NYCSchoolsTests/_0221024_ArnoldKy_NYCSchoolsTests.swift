//
//  _0221024_ArnoldKy_NYCSchoolsTests.swift
//  20221024-ArnoldKy-NYCSchoolsTests
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import XCTest
@testable import _0221024_ArnoldKy_NYCSchools

class _0221024_ArnoldKy_NYCSchoolsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSchoolFetch() async {
        let vm = SchoolsViewModel(router: .init())
        do {
            try await vm.fetch()
            XCTAssert(vm.schools.count > 1)
            //dispatchGroup.leave()
        } catch {
            XCTFail("Unable to perform network test")
        }
    }
    
    func testSATFetch() async {
        let vm = SchoolsViewModel(router: .init())
        do {
            try await vm.fetch()
            var numberOfMatches = 0
            let numberOfSchools = vm.schools.count
            let loops = min(10, numberOfSchools)
            for index in 0..<loops {
                let school = vm.schools[index]
                let scores = try await vm.testScoresForSchool(model: school)
                if scores != nil {
                    numberOfMatches += 1
                }
            }
            XCTAssert(numberOfMatches > 0)
        } catch {
            XCTFail("Unable to perform network test")
        }
    }
}
